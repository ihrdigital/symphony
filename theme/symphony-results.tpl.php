<?php
/**
 * @file
 * Template file for the symphony table results display.
 *
 * Variables available:
 * $results: The results array (the SOAP response transformed into an array)
 *
 * The table header is always needed, even if empty, for the pagination plugin
 *
 */
?>
<div id="sym-eventslist">
  <?php if (count($results) == 0): ?>
    <p><?php print t('No events found.'); ?></p>
  <?php else: ?>
    <?php foreach ($results as $result): ?>
      <div class="sym-event-row">
        <div class="sym-event-image">
          <?php if (!empty($result['ImageThumbLink'])): ?>
            <?php
              $image = theme('image', array(
                'path' => $result['ImageThumbLink'],
                'alt' => $result['Title'],
				'width' => 200
              ));
              print render($image);
            ?>
          <?php endif; ?>
        </div>

        <div class="sym-event-details">
          <strong class='sym-event-name sym-event-item'>
            <?php print render($result['event_display_link']); ?>
          </strong>

          <div class="sym-event-desc sym-event-item">
            <?php print $result['ResultDescription']; ?>
          </div>

          <div class="sym-event-dets sym-event-item">
            <?php if (!empty($result['institute_link']) && !$result['institute_link']['#printed']): ?>
              <?php print drupal_render($result['institute_link']); ?><br />
            <?php endif; ?>
            <?php if (!empty($result['series_link']) && !$result['series_link']['#printed']): ?>
              <?php print drupal_render($result['series_link']); ?><br />
            <?php endif; ?>
            <?php if (!empty($result['Code_Type'])): ?>
              <?php print $result['Code_Type']; ?><br />
            <?php endif; ?>
            <?php if (!empty($result['StartDateUnix'])): ?>
              <?php print format_date($result['StartDateUnix'], 'custom', 'd/m/Y'); ?><br />
            <?php endif; ?>
            <?php if (!empty($result['Code_Room'])): ?>
              <?php print $result['Code_Room']; ?><br />
            <?php endif; ?>
          </div>

          <?php if (!empty($result['booking_form_button'])): ?>
            <div class="sym-event-book sym-event-item">
              <?php print drupal_render($result['booking_form_button']); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    <?php endforeach; ?>
  <?php endif; ?>
</div>
