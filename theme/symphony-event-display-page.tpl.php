<?php
/**
 * @file
 * Template file for the symphony Event display page.
 *
 * Variables available:
 * $event: The event array (the SOAP response transformed into an array)
 *
 * If the $event contains the 'WebsitePageContent', we render this instead
 * of rendering the field values specifically. i.e. WebsitePageContent
 * overrides the event detail page content (except for the booking link and
 * content block at the bottom)
 *
 */
?>
<div class="sym-event-page">
  <?php if (!empty($event['WebsitePageContent'])): ?>
    <?php print $event['WebsitePageContent']; ?>
  <?php else: ?>
    <?php if (!empty($event['image_main'])): ?>
      <div class="sym-event-image">
        <?php print drupal_render($event['image_main']); ?>
      </div>
    <?php endif; ?>

    <div class="sym-event-date sym-event-item">
      <strong>Date</strong><br />
      <?php if (!empty($event['StartDate'])): ?>
        <?php print format_date(strtotime($event['StartDate']), 'custom', 'd M Y, H:i'); ?>
      <?php endif; ?>
      <?php if (!empty($event['EndDate'])): ?>
        to <?php print format_date(strtotime($event['EndDate']), 'custom', 'd M Y, H:i'); ?>
      <?php endif; ?>
    </div>

    <div class="sym-event-institute-link sym-event-item">
    <?php if (!empty($event['institute_link'])  && !$event['institute_link']['#printed']): ?>
      <strong>Institute</strong><br />
      <?php print drupal_render($event['institute_link']); ?>
    <?php endif; ?>
    </div>

    <div class="sym-event-code-series sym-event-item">
    <?php if (!empty($event['series_link']) && !$event['series_link']['#printed']): ?>
      <strong>Series</strong><br />
      <?php print drupal_render($event['series_link']); ?>
    <?php endif; ?>
    </div>

    <div class="sym-event-code-type sym-event-item">
    <?php if (!empty($event['Code_Type'])): ?>
      <strong>Type</strong><br />
      <?php print $event['Code_Type']; ?>
    <?php endif; ?>
    </div>

    <div class="sym-event-location sym-event-item">
    <?php if (!empty($event['Location'])): ?>
      <strong>Venue</strong><br />
      <?php print $event['Location']; ?>
    <?php elseif (!empty($event['Code_Room'])): ?>
      <strong>Venue</strong><br />
      <?php print $event['Code_Room']; ?>
    <?php endif; ?>
    </div>

    <div class="sym-event-location sym-event-item">
    <?php if (!empty($event['LongDescription'])): ?>
      <strong>Description</strong><br />
      <?php print $event['LongDescription']; ?>
    <?php elseif (!empty($event['MediumDescription'])): ?>
      <strong>Description</strong><br />
      <?php print $event['MediumDescription']; ?>
    <?php elseif (!empty($event['Description'])): ?>
      <strong>Description</strong><br />
      <?php print $event['Description']; ?>
    <?php endif; ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($event['booking_form_button'])): ?>
    <div class="sym-event-book sym-event-item">
      <?php print drupal_render($event['booking_form_button']); ?>
    </div>
  <?php endif; ?>

  <div class='sym-contactdetails'>
    <h3 class='sym-contactdetails-header'><?php print t('Contact'); ?></h3>
    <?php if (!empty($event['HelplineName'])): ?>
      <?php print $event['HelplineName']; ?><br />
    <?php endif; ?>
    <?php if (!empty($event['HelplineEmail'])): ?>
      <?php print l($event['HelplineEmail'],
        'mailto:' . $event['HelplineEmail'] .
        '?subject=Enquiry about ' . $event['Title']); ?><br />
    <?php endif; ?>
    <?php if (!empty($event['HelplineTelephone'])): ?>
      <?php print $event['HelplineTelephone']; ?><br />
    <?php endif; ?>
  </div>
</div>
