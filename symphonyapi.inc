<?php

function intializeSymphony(){	
	global $soapurl, $apikey, $client, $ns, $symphonyinitialised, $base_address;
	
	$base_address = "https://developer.sym-online.com";
	
	if($symphonyinitialised!==true){
		$ns = $base_address."/API/1_0/SOAP/";
		
		$soapurl = $ns."SymphonyAPI.asmx?WSDL";
		
		$apikey = variable_get("symphony_apikey", '');

		/* Initialize webservice using WSDL address */
		$client = new SoapClient($soapurl, array(
								'cache_wsdl' => WSDL_CACHE_NONE,  //Cache needs to be disabled when using multiple sites else PHP caches the old wdsl file
								'soap_version' => SOAP_1_1,
								'location' => $soapurl,
								'uri'      => $ns						
					));
		
		
		$symphonyinitialised = true;
	}
}

function CreateSignature($operation, $timestamp)
{
	$SecretKey = variable_get("symphony_secretkey", '');
	
	/* 
	 * PHP Conversion of CreateSignature method from http://developer.sym-online.com/API/1_0/help.aspx
	 */
	
	$data = utf8_encode($operation . $timestamp);
	
	$hash = hash_hmac('sha1', $data, utf8_encode($SecretKey), true);

	$signature = base64_encode($hash);

	return $signature;
}

function makeSOAPCall($functionname, $params){	

	global $ns, $client, $apikey, $symphonyinitialised;
	
	if(!$symphonyinitialised){
		intializeSymphony();
	}
	
	/*
	 * DateTime string needs to be of the format yyyy-MM-ddTHH:mm:ss.fffZ (eg. 2010-04-09T09:08Z).  PHP does not support milliseconds as a function, so the time needs to be calculated
	 * then the milliseconds concatenated onto the end as the microseconds divided by 1000.  The format 'P' date adds the timezone on as this is required even server->server if there is a different
	 * in timezones relating to BST since the SOAP server uses UTC only (and obviously is required if actually in different timezones).
	 */
	$t = microtime(true);
	$micro = sprintf("%03d",($t - floor($t)) * 1000000);
	$d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
	$milliseconds = floor($d->format("u")/1000);

	$isodate = $d->format("Y-m-d\TH:i:s");
	
	//Calculate offset from UTC
	$timezonemodifier = $d->format("P");
	
	$isodate .= ".".$milliseconds.$timezonemodifier;

	$signature = CreateSignature($functionname, $isodate);
	

	$soapParameters = array(
		'APIKey' => $apikey,
		'Timestamp' => $isodate,
		'Operation' => $functionname,
		'Signature' => $signature,
		'soapaction' => $ns.$functionname
	);

	$header = new SoapHeader($ns,'AuthenticationHeader',$soapParameters,false);
	$client->__setSoapHeaders($header);
	
	$result = $client->__soapCall($functionname, array($params));
	
	
	return $result;
}

function GetExtEventsDetails($includepast = true){
	$params = array(
		"includePastEvents" => $includepast
	);
	
	try{
	
		$result = makeSOAPCall("GetExtEventsDetails", $params);
		
		$events = array();
		if(is_array($result->GetExtEventsDetailsResult->EventExtendedDetailsResponse)){
			foreach($result->GetExtEventsDetailsResult->EventExtendedDetailsResponse as $event){
				$array = array();
				$array['EventId'] = $event->EventId;
				$array['Title'] = $event->Title;
				
				$array['StartDateUnix'] = strtotime($event->StartDate);
				$array['StartDate'] = $event->StartDate;
				$array['StartTime'] = $event->StartTime;
				$array['EndDate'] = $event->EndDate;
				$array['EndTime'] = $event->EndTime;
				$array['Description'] = $event->Description;
				$array['Status'] = $event->Status;
				$array['Location'] = $event->Location;
				$array['Organisers'] = $event->Organisers;
				$array['Type'] = $event->Type;
				$array['DefaultWebsitePageFriendlyName'] = $event->DefaultWebsitePageFriendlyName;
				$array['DefaultWebsiteID'] = $event->DefaultWebsiteID;
				$array['DefaultBookingFormUrl'] = $event->DefaultBookingFormUrl;
				$array['DefaultBookingFormName'] = $event->DefaultBookingFormName;
				$events[$event->EventId] = $array;
			}
		}
	}catch (SoapFault $e){
		return getFormattedOutputError($e);
	}
	
	//Has to return an array rather than the original object in order to sort the results in php
	return $events;
}

function orderAssArrayBy($data, $field, $sortdir = "asc"){
	$code = "return strnatcmp(\$a['$field'], \$b['$field']);";
	if($sortdir=='asc'){
		usort($data, create_function('$a,$b', $code));
	}else{
		usort($data, create_function('$b,$a', $code));
	}
	return $data;
}

function GetWebsitePageContent($DefaultWebsiteID, $DefaultWebsitePageFriendlyName){
	
	
	
	$params = array(
		"friendlyUrlName" => $DefaultWebsitePageFriendlyName,
		"isTest" => TRUE,
		"websiteId" => $DefaultWebsiteID
	);
	
	try{
		$result = makeSOAPCall("GetWebsitePageContent", $params);
	}catch (SoapFault $e){
		return getFormattedOutputError($e);
	}
		
	return $result->GetWebsitePageContentResult;
}


function GetProfileList(){
	$params = array();
	
	try{
		$result = makeSOAPCall("GetProfileList", null);
		$ProfileTypes = $result->GetProfileListResult->ProfileTypes;

		
		$profiles = array();
		if( property_exists ( $ProfileTypes , "ProfileList" ) ) {
			if(is_array($ProfileTypes->ProfileList)){
				$ProfileTypesarray = $ProfileTypes->ProfileList;	
				foreach($ProfileTypesarray as $ProfileType){
					$array = array();
					$array['ClientCompanyId'] = $ProfileType->ClientCompanyId;
					$array['ProfileTypeId'] = $ProfileType->ProfileTypeId;
					$array['ProfileType'] = $ProfileType->ProfileType;
					if( property_exists ( $ProfileType->ProfileValues , "ProfileValue" ) ) {
						$ProfileValues = $ProfileType->ProfileValues->ProfileValue;
						if(is_array($ProfileValues)){
							foreach($ProfileValues as $ProfileValue){
								$array2 = array();
								$array2['Id'] = $ProfileValue->Id;
								$array2['Name'] = $ProfileValue->Name;
								$array['ProfileValues'][] = $array2;
							}
						}else{
							$array2 = array();
							$array2['Id'] = $ProfileValues->Id;
							$array2['Name'] = $ProfileValues->Name;
							$array['ProfileValues'][] = $array2;
						}
					}else{
						$array['ProfileValues'][] = array();
					}
					$profiles[] = $array;
				}
			}else{
				$ProfileType = $ProfileTypes->ProfileList;	
				$array = array();
				$array['ClientCompanyId'] = $ProfileType->ClientCompanyId;
				$array['ProfileTypeId'] = $ProfileType->ProfileTypeId;
				$array['ProfileType'] = $ProfileType->ProfileType;
				$ProfileValues = $ProfileType->ProfileValues->ProfileValue;
				if(!is_null($ProfileValues)){
					if(is_array($ProfileValues)){
						foreach($ProfileValues as $ProfileValue){
							$array2 = array();
							$array2['Id'] = $ProfileValue->Id;
							$array2['Name'] = $ProfileValue->Name;
							$array['ProfileValues'][] = $array2;
						}
					}else{
						$array2 = array();
						$array2['Id'] = $ProfileValues->Id;
						$array2['Name'] = $ProfileValues->Name;
						$array['ProfileValues'][] = $array2;
					}
				}else{
					$array['ProfileValues'][] = array();
				}
				$profiles[] = $array;
				
			}
		}
	}catch (SoapFault $e){
		return getFormattedOutputError($e);
	}
	
	return $profiles;
}  


function EventProfileSearch($searchQuery){
	
	$searchQuery = htmlspecialchars($searchQuery);
	
	$params = array(
		"searchQuery" => "WHERE $searchQuery"
	);
	
	try{
		$result = makeSOAPCall("EventProfileSearch", $params);
		
		$events = array();
		if( property_exists ( $result->EventProfileSearchResult->Events , "EventModel" ) ) { 
			$ResultEvents = $result->EventProfileSearchResult->Events->EventModel;
			if(is_array($ResultEvents)){
				foreach($ResultEvents as $event){
					$array = array();
					$array['EventId'] = $event->Event->EventId;
					$array['Title'] = $event->Event->Title;
					
					$array['StartDateUnix'] = strtotime($event->Event->StartDate);
					$array['StartDate'] = $event->Event->StartDate;
					$array['EndDate'] = $event->Event->EndDate;
					$array['Description'] = $event->Event->Description;
					$array['EventStatus'] = $event->Event->EventStatus;
					$array['Location'] = $event->Event->Location;
					$array['Type'] = $event->Event->Type;
					$events[$event->Event->EventId] = $array;
				}
			}else{
				$event = $ResultEvents;
				
				$array = array();
				
				$array['EventId'] = $event->Event->EventId;
				$array['Title'] = $event->Event->Title;
				$array['StartDateUnix'] = strtotime($event->Event->StartDate);
				$array['StartDate'] = $event->Event->StartDate;
				$array['EndDate'] = $event->Event->EndDate;
				$array['Description'] = $event->Event->Description;
				$array['EventStatus'] = $event->Event->EventStatus;
				$array['Location'] = $event->Event->Location;
				$array['Type'] = $event->Event->Type;
				$events[$event->Event->EventId] = $array;
			}
		}
	}catch (SoapFault $e){
		return getFormattedOutputError($e);
	}
	
	return $events;

}

function getEventDetails($eventid){
	if(!is_numeric($eventid)){
		return "Event ID not set";
	}
	
	$params = array(
		"eventId" => $eventid
	);

	$event = array();
	try{
		$result = makeSOAPCall("GetEventDetails", $params);
		
		$resultset = $result->GetEventDetailsResult;
		
		$event['Description'] = $resultset->Description;
		$event['EndDate'] = $resultset->EndDate;
		$event['Capacity'] = $resultset->Capacity;
		$event['HasCapacityLimits'] = $resultset->HasCapacityLimits;
		$event['HelplineEmail'] = $resultset->HelplineEmail;
		$event['HelplineName'] = $resultset->HelplineName;
		$event['HelplineTelephone'] = $resultset->HelplineTelephone;
		$event['IsArchived'] = $resultset->IsArchived;
		$event['SignupStartDate'] = $resultset->SignupStartDate;
		$event['StartDate'] = $resultset->StartDate;
		$event['Title'] = $resultset->Title;
		$event['AttendeeCount'] = $resultset->AttendeeCount;
		$event['Venues'] = $resultset->Venues;
	}catch (SoapFault $e){
		return "Event not found";
	}
	
	return $event;
}


function getEventDetails2($eventid){
	if(!is_numeric($eventid)){
		return "Event ID not set";
	}
	
	$params = array(
		"eventId" => $eventid
	);

	$event = array();
	try{
		$result = makeSOAPCall("GetEventDetails2", $params);
		
		$resultset = $result->GetEventDetails2Result;
		$event['EventId'] = $resultset->EventId;
		$event['Description'] = $resultset->Description;
		$event['EndDate'] = $resultset->EndDate;
		$event['Capacity'] = $resultset->Capacity;
		$event['HasCapacityLimits'] = $resultset->HasCapacityLimits;
		$event['HelplineEmail'] = $resultset->HelplineEmail;
		$event['HelplineName'] = $resultset->HelplineName;
		$event['HelplineTelephone'] = $resultset->HelplineTelephone;
		$event['IsArchived'] = $resultset->IsArchived;
		$event['SignupStartDate'] = $resultset->SignupStartDate;
		$event['StartDate'] = $resultset->StartDate;
		$event['Title'] = $resultset->Title;
		$event['AttendeeCount'] = $resultset->AttendeeCount;
		$event['Venues'] = $resultset->Venues;
		$event['ThirdPartyEvent'] = $resultset->ThirdPartyEvent;
		$event['ImageLink'] = $resultset->ImageLink;
		$event['ImageThumbLink'] = $resultset->ImageThumbLink;
		$event['Code_Type'] = $resultset->Code_Type;
		$event['Code_Series'] = $resultset->Code_Series;
		$event['CPDValue'] = $resultset->CPDValue;
		$event['DefaultBookingFormName'] = $resultset->DefaultBookingFormName;
		$event['DefaultBookingFormUrl'] = $resultset->DefaultBookingFormUrl;
		$event['DefaultWebsiteID'] = $resultset->DefaultWebsiteID;
		$event['DefaultWebsiteName'] = $resultset->DefaultWebsiteName;
		$event['DefaultWebsitePageFriendlyName'] = $resultset->DefaultWebsitePageFriendlyName;
		$event['StartTime'] = $resultset->StartTime;
		$event['EndTime'] = $resultset->EndTime;
		$event['Location'] = $resultset->Location;
		$event['Organisers'] = $resultset->Organisers;
		if( property_exists ( $resultset , "ExternalURL" ) ) { $event['ExternalURL'] = $resultset->ExternalURL; } else { $event['ExternalURL'] = ""; }
		if( property_exists ( $resultset , "ImageThumbLink" ) ) { $event['ImageThumbLink'] = $resultset->ImageThumbLink; } else { $event['ImageThumbLink'] = ""; }
		if( property_exists ( $resultset , "Code_Room" ) ) { $event['Code_Room'] = $resultset->Code_Room; } else { $event['Code_Room'] = ""; }
		if( property_exists ( $resultset , "MediumDescription" ) ) { $event['MediumDescription'] = $resultset->MediumDescription; } else { $event['MediumDescription'] = ""; }
		if( property_exists ( $resultset , "LongDescription" ) ) { $event['LongDescription'] = $resultset->LongDescription; } else { $event['LongDescription'] = ""; }
		if( property_exists ( $resultset , "ClientName" ) ) { $event['ClientName'] = $resultset->ClientName; } else { $event['ClientName'] = ""; }
		if( property_exists ( $resultset , "Type" ) ) { $event['Type'] = $resultset->Type; } else { $event['Type'] = ""; }
		if( property_exists ( $resultset , "ClientCompanyName" ) ) { $event['ClientCompanyName'] = $resultset->ClientCompanyName; } else { $event['ClientCompanyName'] = ""; }
	}catch (SoapFault $e){
		return "No event found";
	}
	
	return $event;
}


/*
 * Returns a searchable set of events listed on the system
 * 
 * clientCompanyId: The Institution ID
 * startDate: Unix Timestamp
 * endDate: Unix timestamp
 * eventType: Category?  See GetProfileList
 * eventTitle
 * 
 */
function GetExtEventsDetails2($clientCompanyId, $startDate, $endDate, $eventType, $eventTitle){
	
	
	if($startDate==null){
		$formattedStart = null;
	}else{
		$formattedStart = substr(date("c", $startDate), 0, -6);
	}
	if($endDate==null){
		$formattedEnd = null;
	}else{
		$formattedEnd = substr(date("c", $endDate), 0, -6);
	}
	
	
	$params = array(
		"clientCompanyId" => $clientCompanyId,
		"startDate" => $formattedStart,
		"endDate" => $formattedEnd,
		"eventType" => $eventType,
		"eventTitle" => $eventTitle
	);
	
	try{
		$result = makeSOAPCall("GetExtEventsDetails2", $params);
		
		$events = array();
		if( property_exists ( $result->GetExtEventsDetails2Result , "EventExtendedDetailsResponse2" ) ) { 
			if(is_array($result->GetExtEventsDetails2Result->EventExtendedDetailsResponse2)){
				foreach($result->GetExtEventsDetails2Result->EventExtendedDetailsResponse2 as $event){
					
					$array = array();
					$array['EventId'] = $event->EventId;
					$array['Title'] = $event->Title;
					
					$array['StartDateUnix'] = strtotime($event->StartDate);
					$array['StartDate'] = $event->StartDate;
					$array['StartTime'] = $event->StartTime;
					$array['EndDate'] = $event->EndDate;
					$array['EndTime'] = $event->EndTime;
					$array['Description'] = $event->Description;
					$array['Status'] = $event->Status;
					$array['Location'] = $event->Location;
					$array['Organisers'] = $event->Organisers;
					$array['CPDValue'] = $event->CPDValue;
					$array['DefaultWebsitePageFriendlyName'] = $event->DefaultWebsitePageFriendlyName;
					$array['DefaultBookingFormUrl'] = $event->DefaultBookingFormUrl;
					$array['DefaultBookingFormName'] = $event->DefaultBookingFormName;
					$array['DefaultWebsiteID'] = $event->DefaultWebsiteID;
					$array['ThirdPartyEvent'] = $event->ThirdPartyEvent;
					$array['HelplineName'] = $event->HelplineName;
					$array['HelplineTelephone'] = $event->HelplineTelephone;
					$array['HelplineEmail'] = $event->HelplineEmail;
					$array['ImageLink'] = $event->ImageLink;
					$array['ImageThumbLink'] = $event->ImageThumbLink;
					$array['Code_Type'] = $event->Code_Type;
					$array['ClientCompanyName'] = $event->ClientCompanyName;
					if( property_exists ( $event , "ClientName" ) ) { $array['ClientName'] = $event->ClientName; } else { $array['ClientName'] = ""; }
					if( property_exists ( $event , "Code_Room" ) ) { $array['Code_Room'] = $event->Code_Room; } else { $array['Code_Room'] = ""; }
					if( property_exists ( $event , "Type" ) ) { $array['Type'] = $event->Type; } else { $array['Type'] = ""; }
					if( property_exists ( $event , "ExternalURL" ) ) { $array['ExternalURL'] = $event->ExternalURL; } else { $array['ExternalURL'] = ""; }
					$events[$event->EventId] = $array;
				}
			}else{
				$event = $result->GetExtEventsDetails2Result->EventExtendedDetailsResponse2;
				
				$array = array();
				$array['EventId'] = $event->EventId;
				$array['Title'] = $event->Title;
					
				$array['StartDateUnix'] = strtotime($event->StartDate);
				$array['StartDate'] = $event->StartDate;
				$array['StartTime'] = $event->StartTime;
				$array['EndDate'] = $event->EndDate;
				$array['EndTime'] = $event->EndTime;
				$array['Description'] = $event->Description;
				$array['Status'] = $event->Status;
				$array['Location'] = $event->Location;
				$array['Organisers'] = $event->Organisers;
				$array['CPDValue'] = $event->CPDValue;
				$array['DefaultWebsitePageFriendlyName'] = $event->DefaultWebsitePageFriendlyName;
				$array['DefaultBookingFormUrl'] = $event->DefaultBookingFormUrl;
				$array['DefaultBookingFormName'] = $event->DefaultBookingFormName;
				$array['DefaultWebsiteID'] = $event->DefaultWebsiteID;
				$array['ThirdPartyEvent'] = $event->ThirdPartyEvent;
				$array['HelplineName'] = $event->HelplineName;
				$array['HelplineTelephone'] = $event->HelplineTelephone;
				$array['HelplineEmail'] = $event->HelplineEmail;
				$array['ImageLink'] = $event->ImageLink;
				$array['ImageThumbLink'] = $event->ImageThumbLink;
				$array['Code_Type'] = $event->Code_Type;
				$array['ClientCompanyName'] = $event->ClientCompanyName;
				if( property_exists ( $event , "ClientName" ) ) { $array['ClientName'] = $event->ClientName; } else { $array['ClientName'] = ""; }
				if( property_exists ( $event , "Code_Room" ) ) { $array['Code_Room'] = $event->Code_Room; } else { $array['Code_Room'] = ""; }
				if( property_exists ( $event , "Type" ) ) { $array['Type'] = $event->Type; } else { $array['Type'] = ""; }
				if( property_exists ( $event , "ExternalURL" ) ) { $array['ExternalURL'] = $event->ExternalURL; } else { $array['ExternalURL'] = ""; }
				$events[$event->EventId] = $array;
			}
		}
	}catch (SoapFault $e){
		return getFormattedOutputError($e);
	}
	
	return $events;
}

function GetClientCompanyList(){
	
	try{
		$result = makeSOAPCall("GetClientCompanyList", null);
		
		$resultset = $result->GetClientCompanyListResult->ClientCompanyResponse;
		$clients = array();
		
		if(!is_null($resultset)){
			if(is_array($resultset)){
				foreach($resultset as $result){
					$clients[$result->Id] = $result->Name;
				}
			}else{
				$clients[$resultset->Id] = $resultset->Name;
			}
		}
	}catch (SoapFault $e){
		return getFormattedOutputError($e);
	}
	
	return $clients;
}

function GetEventTypeList(){
	try{
		$result = makeSOAPCall("GetEventTypeList", null);
			
		$resultset = $result->GetEventTypeListResult->EventType;
		$types = array();
		
		if( property_exists ( $resultset , "string" ) ) { 
			if(is_array($resultset->string)){
				foreach($resultset->string as $thisresult){
					$types[$thisresult] = $thisresult;
				}
			}else{
				$types[$resultset->string] = $resultset->string;
			}
		}
	}catch (SoapFault $e){
		return getFormattedOutputError($e);
	}
	
	return $types;
}

function GetEventSpeakers($eventid){
	if(!is_numeric($eventid)){
		return "Event ID not set";
	}
	
	try{
		$result = makeSOAPCall("GetEventSpeakers", null);
		
		$speakers = array();
		
		if( property_exists ($result->GetEventSpeakersResult , "EventSpeakerResponse" ) ) {
			if(is_array($result->GetEventSpeakersResult->EventSpeakerResponse)){
				foreach($result->GetEventSpeakersResult->EventSpeakerResponse as $speaker){

					$array = array();
					if( property_exists ( $speaker , "SpeakerID" ) ) { $array['SpeakerID'] = $speaker->SpeakerID; } else { $array['SpeakerID'] = ""; }
					if( property_exists ( $speaker , "ContactID" ) ) { $array['ContactID'] = $speaker->ContactID; } else { $array['ContactID'] = ""; }
					if( property_exists ( $speaker , "Title" ) ) { $array['Title'] = $speaker->Title; } else { $array['Title'] = ""; }
					if( property_exists ( $speaker , "Forename" ) ) { $array['Forename'] = $speaker->Forename; } else { $array['Forename'] = ""; }
					if( property_exists ( $speaker , "Surname" ) ) { $array['Surname'] = $speaker->Surname; } else { $array['Surname'] = ""; }
					if( property_exists ( $speaker , "FullName" ) ) { $array['FullName'] = $speaker->FullName; } else { $array['FullName'] = ""; }
					if( property_exists ( $speaker , "Duration" ) ) { $array['Duration'] = $speaker->Duration; } else { $array['Duration'] = ""; }
					if( property_exists ( $speaker , "Order" ) ) { $array['Order'] = $speaker->Order; } else { $array['Order'] = ""; }
					if( property_exists ( $speaker , "Organisation" ) ) { $array['Organisation'] = $speaker->Organisation; } else { $array['Organisation'] = ""; }
					if( property_exists ( $speaker , "Occupation" ) ) { $array['Occupation'] = $speaker->Occupation; } else { $array['Occupation'] = ""; }
					if( property_exists ( $speaker , "Email" ) ) { $array['Email'] = $speaker->Email; } else { $array['Email'] = ""; }
					if( property_exists ( $speaker , "Telephone" ) ) { $array['Telephone'] = $speaker->Telephone; } else { $array['Telephone'] = ""; }
					if( property_exists ( $speaker , "Mobile" ) ) { $array['Mobile'] = $speaker->Mobile; } else { $array['Mobile'] = ""; }
					if( property_exists ( $speaker , "Honours" ) ) { $array['Honours'] = $speaker->Honours; } else { $array['Honours'] = ""; }
					if( property_exists ( $speaker , "Biography" ) ) { $array['Biography'] = $speaker->Biography; } else { $array['Biography'] = ""; }
					if( property_exists ( $speaker , "FacebookURL" ) ) { $array['FacebookURL'] = $speaker->FacebookURL; } else { $array['FacebookURL'] = ""; }
					if( property_exists ( $speaker , "TwitterURL" ) ) { $array['TwitterURL'] = $speaker->TwitterURL; } else { $array['TwitterURL'] = ""; }
					if( property_exists ( $speaker , "LinkedInURL" ) ) { $array['LinkedInURL'] = $speaker->LinkedInURL; } else { $array['LinkedInURL'] = ""; }
					if( property_exists ( $speaker , "GoogleURL" ) ) { $array['GoogleURL'] = $speaker->GoogleURL; } else { $array['GoogleURL'] = ""; }
					if( property_exists ( $speaker , "WebsiteURL" ) ) { $array['WebsiteURL'] = $speaker->WebsiteURL; } else { $array['WebsiteURL'] = ""; }
					if( property_exists ( $speaker , "ShortBio" ) ) { $array['ShortBio'] = $speaker->ShortBio; } else { $array['ShortBio'] = ""; }
						
					$speakers[$speaker->SpeakerID] = $array;
				}
				
			}
		}
	}catch (SoapFault $e){
		return getFormattedOutputError($e);
	}
	
	return $speakers;
}



/*
 * Returns a searchable set of events listed on the system sorted by date ascending
 * 
 * clientCompanyId: The Institution ID
 * startDate: Unix Timestamp.  If set to null, defaults to current date
 * endDate: Unix timestamp
 * eventType: String - See GetProfileList
 * eventTitle
 * subClientCompany: String.  See GetSubClientCompanyList
 * pageNumber: must be greater than or equal to 1 otherwise everything is returned
 * resultCount: results per page
 * 
 */
function GetExtEventsDetails3($clientCompanyId = null, $startDate = null, $endDate = null, $eventType = null, $eventTitle = null, $subClientCompany = null, $pageNumber = 1, $resultCount = 10, $profileSearchArray = null, $series = null){

	if($startDate==null){
		$formattedStart = substr(date("c"), 0, -6);
	}else{
		$formattedStart = substr(date("c", $startDate), 0, -6);
	}
	if($endDate==null){
		$formattedEnd = null;
	}else{
		$formattedEnd = substr(date("c", $endDate), 0, -6);
	}
	
	if(!is_array($profileSearchArray)){
		$profileSearchArray = null;
	}
	
	$params = array(
		"clientCompanyId" => $clientCompanyId,
		"startDate" => $formattedStart,
		"endDate" => $formattedEnd,
		"eventType" => $eventType,
		"eventTitle" => $eventTitle,
		"subClientCompany" => $subClientCompany,
		"pageNumber" => $pageNumber,
		"resultCount" => $resultCount,
		"profileSearchArray" => $profileSearchArray,
		"series" => $series
	);
	
	try{
		$result = makeSOAPCall("GetExtEventsDetails3", $params);
		$events = array();
		if( property_exists ( $result->GetExtEventsDetails3Result , "Events" ) ) { 
			$events['numresults'] = $result->GetExtEventsDetails3Result->TotalEventCount;
			$events['events'] = array();
			if( property_exists ( $result->GetExtEventsDetails3Result->Events , "EventExtendedDetailsResponse3Details" ) ) { 
				if(is_array($result->GetExtEventsDetails3Result->Events->EventExtendedDetailsResponse3Details)){
					foreach($result->GetExtEventsDetails3Result->Events->EventExtendedDetailsResponse3Details as $event){
						$array = array();
						$array['EventId'] = $event->EventId;
						$array['Title'] = $event->Title;
						
						$array['StartDateUnix'] = strtotime($event->StartDate);
						$array['StartDate'] = $event->StartDate;
						$array['StartTime'] = $event->StartTime;
						$array['EndDate'] = $event->EndDate;
						$array['EndTime'] = $event->EndTime;
						$array['Description'] = $event->Description;
						$array['Status'] = $event->Status;
						$array['Location'] = $event->Location;
						$array['Organisers'] = $event->Organisers;
						$array['CPDValue'] = $event->CPDValue;
						$array['DefaultWebsitePageFriendlyName'] = $event->DefaultWebsitePageFriendlyName;
						$array['DefaultBookingFormUrl'] = $event->DefaultBookingFormUrl;
						$array['DefaultBookingFormName'] = $event->DefaultBookingFormName;
						$array['DefaultWebsiteID'] = $event->DefaultWebsiteID;
						$array['ThirdPartyEvent'] = $event->ThirdPartyEvent;
						$array['HelplineName'] = $event->HelplineName;
						$array['HelplineTelephone'] = $event->HelplineTelephone;
						$array['HelplineEmail'] = $event->HelplineEmail;
						$array['ImageLink'] = removeAdditionalParams($event->ImageLink);
						$array['ImageThumbLink'] = removeAdditionalParams($event->ImageThumbLink);
						$array['Code_Type'] = $event->Code_Type;
						$array['ClientCompanyName'] = $event->ClientCompanyName;
						if( property_exists ( $event , "SubClientCompanyName" ) ) { $array['SubClientCompanyName'] = $event->SubClientCompanyName; } else { $array['SubClientCompanyName'] = ""; }
						if( property_exists ( $event , "ClientName" ) ) { $array['ClientName'] = $event->ClientName; } else { $array['ClientName'] = ""; }
						if( property_exists ( $event , "Code_Room" ) ) { $array['Code_Room'] = $event->Code_Room; } else { $array['Code_Room'] = ""; }
						if( property_exists ( $event , "Type" ) ) { $array['Type'] = $event->Type; } else { $array['Type'] = ""; }
						if( property_exists ( $event , "ExternalURL" ) ) { $array['ExternalURL'] = $event->ExternalURL; } else { $array['ExternalURL'] = ""; }
						if( property_exists ( $event , "Code_Series" ) ) { $array['Code_Series'] = $event->Code_Series; } else { $array['Code_Series'] = ""; }
						$events['events'][$event->EventId] = $array;
					}
				}else{
					$event = $result->GetExtEventsDetails3Result->Events->EventExtendedDetailsResponse3Details;
					
					$array = array();
					$array['EventId'] = $event->EventId;
					$array['Title'] = $event->Title;
						
					$array['StartDateUnix'] = strtotime($event->StartDate);
					$array['StartDate'] = $event->StartDate;
					$array['StartTime'] = $event->StartTime;
					$array['EndDate'] = $event->EndDate;
					$array['EndTime'] = $event->EndTime;
					$array['Description'] = $event->Description;
					$array['Status'] = $event->Status;
					$array['Location'] = $event->Location;
					$array['Organisers'] = $event->Organisers;
					$array['CPDValue'] = $event->CPDValue;
					$array['DefaultWebsitePageFriendlyName'] = $event->DefaultWebsitePageFriendlyName;
					$array['DefaultBookingFormUrl'] = $event->DefaultBookingFormUrl;
					$array['DefaultBookingFormName'] = $event->DefaultBookingFormName;
					$array['DefaultWebsiteID'] = $event->DefaultWebsiteID;
					$array['ThirdPartyEvent'] = $event->ThirdPartyEvent;
					$array['HelplineName'] = $event->HelplineName;
					$array['HelplineTelephone'] = $event->HelplineTelephone;
					$array['HelplineEmail'] = $event->HelplineEmail;
					$array['ImageLink'] = removeAdditionalParams($event->ImageLink);
					$array['ImageThumbLink'] = removeAdditionalParams($event->ImageThumbLink);
					$array['Code_Type'] = $event->Code_Type;
					$array['ClientCompanyName'] = $event->ClientCompanyName;
					if( property_exists ( $event , "SubClientCompanyName" ) ) { $array['SubClientCompanyName'] = $event->SubClientCompanyName; } else { $array['SubClientCompanyName'] = ""; }
					if( property_exists ( $event , "ClientName" ) ) { $array['ClientName'] = $event->ClientName; } else { $array['ClientName'] = ""; }
					if( property_exists ( $event , "Code_Room" ) ) { $array['Code_Room'] = $event->Code_Room; } else { $array['Code_Room'] = ""; }
					if( property_exists ( $event , "Type" ) ) { $array['Type'] = $event->Type; } else { $array['Type'] = ""; }
					if( property_exists ( $event , "ExternalURL" ) ) { $array['ExternalURL'] = $event->ExternalURL; } else { $array['ExternalURL'] = ""; }
					if( property_exists ( $event , "Code_Series" ) ) { $array['Code_Series'] = $event->Code_Series; } else { $array['Code_Series'] = ""; }
					$events['events'][$event->EventId] = $array;
				}
			}
		}
	}catch (SoapFault $e){
		return getFormattedOutputError($e);
	}

	return $events;
}

function GetSubClientCompanyList(){
	
	try{
		$result = makeSOAPCall("GetSubClientCompanyList", null);
	
		$companies = array();
		if( property_exists ( $result->GetSubClientCompanyListResult , "GetSubClientCompanyListResponse" ) ) {
			if(is_array($result->GetSubClientCompanyListResult->GetSubClientCompanyListResponse)){
				foreach($result->GetSubClientCompanyListResult->GetSubClientCompanyListResponse as $company){
					if(!array_key_exists($company->ClientCompanyId, $companies)){
						$companies[$company->ClientCompanyId] = array();
						$companies[$company->ClientCompanyId]['ClientCompanyName'] = $company->ClientCompanyName;
						$companies[$company->ClientCompanyId]['subClients'] = array();
					}
					$companies[$company->ClientCompanyId]['subClients'][] = $company->Name;
				}
			}
		}else{
			$company = $result->GetSubClientCompanyListResult->GetSubClientCompanyListResponse;
			
			if(!array_key_exists($company->ClientCompanyId, $companies)){
				$companies[$company->ClientCompanyId] = array();
				$companies[$company->ClientCompanyId]['ClientCompanyName'] = $company->ClientCompanyName;
				$companies[$company->ClientCompanyId]['subClients'] = array();
			}
			$companies[$company->ClientCompanyId]['subClients'][] = $company->Name;
		}
	}catch (SoapFault $e){
		return getFormattedOutputError($e);
	}
	// dpm($companies, 'companies');
	return $companies;
}

function getFormattedOutputError($error){
	if(strpos($error, "Invalid API key")){
		return "The API Key has not been set correctly in the module configuration";
	}elseif(strpos($error, "Signature is incorrect")){
		return "One of either the API Key or the Secret Key has not been set correctly in the module configuration";
	}
	
	//log the error here
	return "An error occured, please contact an administrator. $error";
}

function removeAdditionalParams($link){
	//remove all additional parameters after the ?
	return current(explode("?", $link));
}
