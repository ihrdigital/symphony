<?php

/**
 * SAS Events Symphony itegration
 * This module overrides some of the Symphony API module behaviour,
 * linking events displayed on the SAS Website to institute websites.
 */

/**
 * Get the array of clientid->domain name string map
 * but cache it, because it will be called for 
 * all links when they are preprocessed
 *
 * @param  boolean $enabled_only Only return values enabled in the admin settings
 */
function _symphony_sas_get_domain_map($enabled_only = FALSE) {
  $clientid_domain_map = &drupal_static(__FUNCTION__);

  if(!isset($clientid_domain_map)) {
    // Manual mapping of Symphony client ID's to domain names used for rewriting.
    $clientid_domain_map = array(
      // Institutes
      '344' => 'http://www.ials.sas.ac.uk',  // Institute of Advanced Legal Studies
      '345' => 'http://www.icls.sas.ac.uk',  // Institute of Classical Studies
      '346' => 'http://commonwealth.sas.ac.uk',  // Institute of Commonwealth Studies
      '347' => 'http://www.ies.sas.ac.uk',  // Institute of English Studies
      '365' => 'http://www.history.ac.uk',  // Institute of Historical Research
      '366' => 'http://ilas.sas.ac.uk',  // Institute of Latin American Studies
      '367' => 'http://modernlanguages.sas.ac.uk',  // Institute of Modern Languages Research
      '368' => 'http://www.philosophy.sas.ac.uk',  // Institute of Philosophy
      '343' => 'http://www.sas.ac.uk',  // SAS Central
      '369' => 'http://www.warburg.sas.ac.uk', // The Warburg Institute

      // Projects/Initiatives
      '375' => 'http://hrc.sas.ac.uk', // Human Rights Constorium
      '376' => 'http://rli.sas.ac.uk'  // Refugee Law Initiative
      //'' => 'http://cps.blogs.sas.ac.uk' // Centre for Postcolonial Studies
      //'' => 'http://humanmind.ac.uk' // Human Mind Project
      //'' => 'http://cities.blogs.sas.ac.uk' // Cities @SAS
      //'' => 'http://beinghumanfestival.org' // Being Human
    );

    if ($enabled_only) {  // Filter array to only return enabled values
      $config = array_flip(variable_get('symphony_sas_domain_rewriting'));
      $clientid_domain_map = array_intersect_key($clientid_domain_map, $config);
    }
  }
  return $clientid_domain_map;
}


/**
 * Implements hook_preprocess_HOOK
 * Preprocesses the event display page to remove institute link.
 */
function symphony_sas_preprocess_symphony_event_display_page(&$vars) {
  if (!empty($vars['event']['institute_link'])) {
    _symphony_sas_unlink_institute_link($vars['event']['institute_link']);
  }
}


/**
 * Implements hook_preprocess_HOOK
 * Preprocesses the event spotlight to remove institute link.
 */
function symphony_sas_preprocess_symphony_spotlight(&$vars) {
  if (!empty($vars['event']['institute_link'])) {
    _symphony_sas_unlink_institute_link($vars['event']['institute_link']);
  }
}


/**
 * Implements custom HOOK_event_display_link_alter()
 * Alters an event's event_display_link path injecting SAS domain names as a prefix
 */
function symphony_sas_event_display_link_alter(&$link, $event) {
  if (empty($event['ExternalURL'])) {
    $clientid_domain_map = _symphony_sas_get_domain_map($enabled_only=TRUE);
    if (isset($clientid_domain_map[$event['ClientCompanyId']])) {
      $link['#path'] = $clientid_domain_map[$event['ClientCompanyId']] . $link['#path'];
      show($link);
    }
  }
}


/**
 * Implements custom HOOK_institute_link_alter()
 * Shows the institute link.
 */
function symphony_sas_institute_link_alter(&$link, $event) {
  show($link);
}


/**
 * Implements custom HOOK_series_link_alter()
 * Hide the series link on the SAS website.
 */
function symphony_sas_series_link_alter(&$link, $event) {
  hide($link);
}


/**
 * Function to alter in place an event's institute_link and
 * turn it into plain markup.
 */
function _symphony_sas_unlink_institute_link(&$link) {
    $link['#markup'] = $link['#text'];
    unset($link['#theme']);
}


/**
 * Implements HOOK_form_FORM_ID_alter() to inject custom SAS Events module admin settings.
 */
function symphony_sas_form_symphony_admin_form_alter(&$form, &$form_state, $form_id) {
  $clients = _symphony_get_client_company_list();
  $domain_map = _symphony_sas_get_domain_map(FALSE);

  // Merge the domain name map with the client company list,
  // to use as display options in the admin settings page
  foreach ($clients as $client_id => &$client_name) {
    $client_name .= (isset($domain_map[$client_id])? ' => ' . $domain_map[$client_id] : '');
  }

  $form['symphony_sas_domain_rewriting'] = array(
    '#type' => 'checkboxes',
    '#multiple' => FALSE,
    '#title' => t('Rewrite links to Institute Event display pages'),
    '#description' => t('Enable or disable URL link rewriting for the above Institutes (Client Companies)'),
    '#options' => $clients,
    '#default_value' => variable_get('symphony_sas_domain_rewriting'),
  );
}